package de.thejackimonster.ld34.world;

import java.util.ArrayList;
import java.util.List;

import de.thejackimonster.ld34.entity.Entity;
import de.thejackimonster.ld34.entity.Street;
import de.thejackimonster.ld34.entity.event.Event;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.View;

public final class World {

	private final ArrayList<Entity> entities;
	private int streets;

	public World() {
		entities = new ArrayList<Entity>();
	}

	public final void add(Entity entity) {
		entities.add(entity);
	}

	public final void update(float time) {
		streets = 0;
		
		for (int i = entities.size() - 1; i >= 0; i--) {
			if (entities.get(i).removed) {
				entities.remove(i);
				continue;
			}
			
			entities.get(i).update(time);
			
			if (entities.get(i) instanceof Street) {
				streets++;
			}
		}
	}

	public final void render(Screen screen, View view) {
		for (final Entity entity : entities) {
			entity.render(screen, view);
		}
	}

	public final float topAt(List<Event> events, Entity entity) {
		float top = Float.NaN, y;
		
		for (final Entity e : entities) {
			if ((e != entity) && (!e.removed)) {
				y = e.topAt(events, entity.bounds);
				
				if ((!Float.isNaN(y)) && ((Float.isNaN(top)) || (y < top))) {
					top = y;
				}
			}
		}
		
		return top;
	}

	public final int streetCount() {
		return streets;
	}

}
