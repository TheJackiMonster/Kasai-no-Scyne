package de.thejackimonster.ld34.gfx;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public final class Sprite {

	public static final int SIZE = 32;

	public static Sprite SCYNE;
	public static Sprite BACKGROUND;
	public static Sprite PARTICLE;
	public static Sprite WALL;
	public static Sprite ROCK;
	public static Sprite FIRE;
	public static Sprite HEALTH;
	public static Sprite START;

	private final BufferedImage image;

	private Sprite(String name) throws IOException {
		image = ImageIO.read(Sprite.class.getResource("/content/gfx/" + name + ".png"));
	}

	protected final void draw(Graphics gfx, int action, int x, int y, int width, int height) {
		gfx.drawImage(image, x, y, x + width, y + height, 0, action * image.getWidth(), image.getWidth(), (action + 1) * image.getWidth(), null);
	}

	static {
		try {
			SCYNE = new Sprite("scyne");
			BACKGROUND = new Sprite("background");
			PARTICLE = new Sprite("particle");
			WALL = new Sprite("wall");
			ROCK = new Sprite("rock");
			FIRE = new Sprite("fire");
			HEALTH = new Sprite("health");
			START = new Sprite("start");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
