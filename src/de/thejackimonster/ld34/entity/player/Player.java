package de.thejackimonster.ld34.entity.player;

import de.thejackimonster.ld34.entity.Entity;

public abstract class Player extends Entity {

	public static final float SPEED = 5F;
	public static final float MAX_SPEED = 50F;

	public float speed;
	public float top;
	protected float offset;
	public Mode mode;
	public int health;
	public int points;
	public int crashes;

	public Player(String name) {
		super();
		
		health = 3;
		points = 0;
		crashes = 0;
		
		reset();
	}

	public void reset() {
		speed = SPEED;
		top = Float.NaN;
		offset = 0;
		mode = Mode.AIR;
		
		position[0] = +1.5F;
		position[1] = -0.5F;
		size[0] = 1.5F;
		size[1] = 1.5F;
		
		motion[0] = 0;
		motion[1] = 0;
		
		removed = false;
	}

	public void update(float time) {
		motion[0] = (speed * time + motion[0] * (1 - time));
		motion[1] = Math.min(motion[1] + 9.81F * time, 10);
		
		final float x = position[0] + motion[0] * time;
		final float y = position[1] + motion[1] * time;
		
		if ((mode == Mode.FIRE) && (motion[0] <= speed * 1.5F)) {
			mode = Mode.AIR;
		}
		
		if ((mode == Mode.ROCKET) && (motion[1] >= 10)) {
			mode = Mode.AIR;
		}
		
		position[0] = x;
		
		if (!Float.isNaN(top)) {
			if (y < top - size[1] + offset) {
				position[1] = y;
			} else {
				position[1] = top - size[1] + offset;
				
				if (mode != Mode.FIRE) {
					mode = Mode.GROUND;
				}
			}
		} else {
			position[1] = y;
		}
	}

	public abstract void action(Action action);

}
