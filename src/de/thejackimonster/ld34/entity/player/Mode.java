package de.thejackimonster.ld34.entity.player;

public enum Mode {

	AIR,
	GROUND,
	JUMP,
	FIRE,
	ROCKET;

}
