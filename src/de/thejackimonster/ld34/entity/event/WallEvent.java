package de.thejackimonster.ld34.entity.event;

import de.thejackimonster.ld34.entity.Wall;
import de.thejackimonster.ld34.entity.particle.DustParticle;
import de.thejackimonster.ld34.entity.player.Mode;
import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.world.World;

public final class WallEvent extends Event {

	private final Wall parent;
	private final boolean broken;

	public WallEvent(Wall wall, boolean breakWall) {
		super();
		parent = wall;
		broken = breakWall;
	}

	public void dispose(World world, Player player) {
		if (broken) {
			if (player.mode != Mode.FIRE) {
				player.motion[0] *= 0.5F;
				player.motion[1] *= 0.5F;
				
				player.speed -= player.speed * 0.025F;
				//player.health--;
				player.crashes++;
			} else
			if (player.speed < Player.MAX_SPEED) {
				player.speed += (Player.MAX_SPEED - player.speed) * 0.0025F;
				//player.points++;
			}
			
			for (int i = 0; i < (int) (1 + parent.size[1] * 10); i++) {
				world.add(new DustParticle(parent, player.motion));
			}
			
			parent.removed = true;
		} else {
			parent.passed = true;
			player.points++;
		}
	}

}
