package de.thejackimonster.ld34.entity.event;

import de.thejackimonster.ld34.entity.Street;
import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.world.World;

public final class HoleEvent extends Event {

	private final Street parent;

	public HoleEvent(Street street) {
		super();
		parent = street;
	}

	public void dispose(World world, Player player) {
		player.motion[0] *= 0.5F;
		
		final float top = parent.topAt(null, player.bounds, true);
		
		if (player.position[1] > top + parent.size[1]) {
			player.removed = true;
		}
	}

}
