package de.thejackimonster.ld34.entity;

import java.util.List;

import de.thejackimonster.ld34.entity.event.Event;
import de.thejackimonster.ld34.entity.event.HoleEvent;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.Stripes;
import de.thejackimonster.ld34.gfx.View;

public final class Street extends Entity {

	public static final int COUNT = 8;
	public static final int SIZE = 200;

	private final float[] data;
	public final boolean isHole;

	public Street(int length, boolean hole) {
		super();
		
		final int type = (int) (Math.random() * 4);
		
		data = new float[length];
		isHole = hole;
		
		for (int i = 0; i < data.length; i++) {
			final float x = ((float) i / data.length);
			
			switch (type) {
			case 0:
				data[i] = -x;
				break;
			case 1:
				data[i] = (x - 1) * (x + 1);
				break;
			case 2:
				data[i] = x * x;
				break;
			case 3:
				data[i] = (float) Math.sqrt(9 - x * x / 9);
				break;
			default:
				data[i] = x;
				break;
			}
		}
		
		position[1] += 1;
		size[0] = (data.length / Sprite.SIZE);
		size[1] = (Stripes.SIZE / Sprite.SIZE);
	}

	public void render(Screen screen, View view) {
		if (isHole) {
			screen.drawStripes(Stripes.STREET_HOLE, 0, data, view.size[1], view.transformRect(bounds));
		} else {
			screen.drawStripes(Stripes.STREET, 0, data, view.size[1], view.transformRect(bounds));
		}
		
		if (position[0] + size[0] < view.position[0]) {
			removed = true;
		}
	}

	public float beginTop() {
		return position[1] + data[0];
	}

	public float topAt(List<Event> events, float[][] bounds, boolean ignoreHoles) {
		final int max = Math.min((int) ((bounds[0][0] + bounds[1][0] - position[0]) * data.length / size[0]), data.length - 1);
		final int min = Math.max((int) ((bounds[0][0] - position[0]) * data.length / size[0]), 0);
		
		float top = Float.NaN;
		
		for (int i = min; i <= max; i++) {
			if ((Float.isNaN(top)) || (data[i] < top)) {
				top = data[i];
			}
		}
		
		if ((!isHole) || (ignoreHoles)) {
			if (Float.isNaN(top)) {
				return Float.NaN;
			} else {
				return position[1] + top;
			}
		} else {
			if (!Float.isNaN(top)) {
				if ((events != null) && (bounds[0][1] > position[1] + top)) {
					events.add(new HoleEvent(this));
				}
			}
			
			return Float.NaN;
		}
	}

	public final float topAt(List<Event> events, float[][] bounds) {
		return topAt(events, bounds, false);
	}

	public float endTop() {
		return position[1] + data[data.length - 1];
	}

}
