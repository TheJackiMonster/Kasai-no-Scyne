package de.thejackimonster.ld34.entity.particle;

import de.thejackimonster.ld34.entity.player.Player;
import de.thejackimonster.ld34.gfx.Screen;
import de.thejackimonster.ld34.gfx.Sprite;
import de.thejackimonster.ld34.gfx.View;

public final class FireParticle extends Particle {

	private final float endSize;

	public FireParticle(Player player) {
		super(player);
		
		endSize = (player.motion[0] / Player.SPEED) * 0.1F;
		
		position[0] = player.position[0] + (float) (player.size[0] * (Math.random() + 4) / 8) - 0.05F;
		position[1] = player.position[1] + (float) (player.size[1] * (Math.random() + 3) / 8) - 0.05F;
		
		size[0] = 0.1F;
		size[1] = 0.1F;
	}

	public void render(Screen screen, View view) {
		screen.drawSprite(Sprite.PARTICLE, id, view.transformRect(bounds));
		
		size[0] += (endSize - size[0]) / 2;
		size[1] += (endSize - size[0]) / 2;
	}

}
