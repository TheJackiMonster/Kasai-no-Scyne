package de.thejackimonster.ld34.entity.particle;

import de.thejackimonster.ld34.entity.Entity;

public abstract class Particle extends Entity {

	protected final int id;
	protected final float limit;

	public Particle(Entity entity) {
		super();
		
		id = (int) (Math.abs(System.nanoTime()) % 4);
		limit = entity.position[1] + entity.size[1];
		
		size[0] = 0.4F * entity.size[0];
		size[1] = 0.4F * entity.size[1];
	}

	public void update(float time) {
		motion[1] = Math.min(motion[1] + 9.81F * time, 10);
		
		final float x = position[0] + motion[0] * time;
		final float y = position[1] + motion[1] * time * 0.1F;
		
		position[0] = x;
		position[1] = y;
		
		if (y >= limit) {
			removed = true;
		}
	}

	public float beginTop() {
		return Float.NaN;
	}

}
