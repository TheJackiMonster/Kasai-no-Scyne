Kasai no Scyne ( 火のScyne ):

This game is a project, made for the 'Ludum Dare 34'.
The theme was 'Growing, Two button controls', so I decided to make this game.
It is a simple side-scroller with the possibility to jump and accelerate.
There is not a real goal, just a highscore.